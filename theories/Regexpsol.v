(*

COMP796 Final Examination 2013

This exam assesses all of the learning outcomes of the unit. Specifically,
it aims to test your consolidated knowledge of using Coq to specify the
meaning of a simple language and prove that the language and programs
written in it satisfy various formal properties.

The exam consists of nine questions marked out of a total of 80 marks.
The number of marks allocated to each question is written at the top of
the question.

CONDITIONS OF EXAM

Answer ALL questions.

This exam is a take-home exam. You can use any materials you like to solve
these questions with the following conditions:

  * You should not communicate with anyone except the unit lecturer about
    the exam. If you wish to ask questions about it, please email me
    (anthony.sloane@mq.edu.au) and I will get back to you as soon as
    possible. Do not post questions on the shared iLearn forums.

  * If you use resources that substantially help you to complete the
    questions, you should acknowledge those resources in your submission
    by citing them, including details of where you obtained them.

SUBMISSION

Submit your exam on iLearn via the "Examination submission" link before the
deadline.

Your submission should be a single zip file containing all of the Coq files
necessary to compile and run your solutions. Specifically, you should include
your modified version of this file Regexp.v that contains your definitions
and proofs. Also, include *any* other files that contain definitions, theorems
and proofs that you use; this category includes any chapters from the Software
Foundations book that you use.

In summary, it should be possible me to compile your Coq files easily
and run your Regexp.v file to check that your definitions compile and
your proofs go through.

MARKING

Your answers will be marked on how well they achieve the aims of the
questions. The emphasis will be on correctness although some marks
will be deducted for excessively complex solutions. Part marks will
be awarded if you complete some cases of a proof but not all of them.

*)

(**********************************************************************

IMPORTANT NOTE: make sure that you "Require" any other modules that you
use. Feel free to use any module from Coq's library if you wish.

**********************************************************************)

Require Export SfLib.

(**********************************************************************

QUESTION 1. A simplified regular expression language (5 marks)

Regular expressions are a well-known pattern matching language that is
widely used in software development. Regular expressions are defined
over an arbitrary alphabet. In this exam, the alphabet is the natural
numbers. Thus, our patterns will match against lists of natural numbers.

For the purposes of this exam, the form of regular expressions is as
follows:

 * epsilon, a regular expression that matches the empty list,
 * a single natural number that matches a list containing just that
   number,
 * a sequence of two regular expressions re1 and re2 that matches
   a list that first matches re1 and, if re1 matches then matches re2
   starting at the point where the re1 match finished, and
 * an alternation of two regular expressions re1 and re2 that either
   matches something that re1 matches, or if re1 doesn't match, matches
   something that re2 matches.

Note: usual regular expression notations include a form for repetition
zero or more times, or possibly one or more times. We omit this form
to simplify matters.

Normal regular expression notation writes epsilon as nothing, the single
case as the alphabet symbol, the sequence case by putting two regular
expressions next to each other, and alternation using a vertical bar.
Parentheses are used to group expressions. E.g., the regular expression

    1(2|3)4

would match a 1, followed by a 2 or a 3, followed by a 4.

We won't use this syntax here except in examples. Instead, you should
define an inductive type regexp whose values are regular expressions.

**********************************************************************)

Inductive regexp := Admitted.

(**********************************************************************

QUESTION 2. Nullability of regular expressions (5 marks)

A regular expression is "nullable" if it can match the empty input.

Define a function re_nullable that takes a regular expression re and
returns the Boolean value true if re is nullable, and false otherwise.

**********************************************************************)

Fixpoint re_nullable (re : regexp) : bool := Admitted.

(**********************************************************************

QUESTION 3. The reverse of a regular expression (5 marks)

The reverse of a regular expression re is a regular expression that
completely matches just those lists that are the reverse of lists
that are completely matched by re. A complete match is one that
doesn't leave any part of the input unmatched.

For example, if re is (1|2)3 which completely matches the lists [1;3]
and [2;3], then the reverse of re would be 3(1|2) which completely
matches the lists [3;1] and [3;2].

Write a function re_reverse that computes the reverse of a regular
expression re.

**********************************************************************)

Fixpoint re_reverse (re : regexp) : regexp := Admitted.

(**********************************************************************

QUESTION 4. Reverse of reverse (5 marks)

State and prove a theorem re_reverse_reverse that shows that applying
your re_reverse function twice gives you back the regular expression
that you started with.

**********************************************************************)

Theorem re_reverse_reverse: Admitted.

(**********************************************************************

QUESTION 5. Nullability of reverse (5 marks)

State and prove a theorem re_rev_nullable that shows that the reverse
of a regular expression re has the same nullability as re.

**********************************************************************)

Theorem re_rev_nullable: Admitted.

(**********************************************************************

QUESTION 6. Matching (15 marks)

Write a function re_match that takes a regular expression re and a list
of nats as input and returns an optional list. If re does not match the
input starting at the beginning, return None. Otherwise return a value
(Some rest) where rest is the part of the input that was not matched.

For example, the regular expression 1(2|3)4 matches a 1, followed by a
2 or a 3, followed by a 4.

If re_match is applied to this regular expression and the input list
[1; 3; 4; 5; 6] it should return the value Some ([5; 6]) since the
regular expression matches [1; 3; 4] and leaves the rest [5; 6].
In contrast if the input list is [1; 3] then the return value should
be None since this regular expression does not match that input.

Note: Make sure that your matching process is deterministic in the
following sense: an alternation regular expression should first try
to match its left sub-expression, only trying to match the right
sub-expression if the left sub-expression fails to match. E.g.,
the expression 1|(12) should match the input [1; 2] returning
[2] since the left sub-expression matches the prefix [1].

**********************************************************************)

Fixpoint re_match (re : regexp) (input : list nat) : Admitted.      

(**********************************************************************

QUESTION 7. Equivalence of regular expressions (10 marks)

A correct implementation of regular expression matching should obey
various algebraic laws. For example, it should be the case that
sequencing a regular expression re1 with the alternation of two others
re2 and re3 is the same as an alternation between the sequence
(re1 re2) and the sequence (re1 re3). This form of distributive law
might be written:

   re1 (re2 | re3) = re1 re2 | re1 re3             (**)

if we assume that sequencing has a higher precedence than alternation.

First, complete the following definition to say that (re_equiv re1 re2)
holds if and only if re1 and re2 produce exactly the same output from
re_match when given any input.

**********************************************************************)

Definition re_equiv (re1 re2 : regexp) : Prop := Admitted.
 

(**********************************************************************

Now, use re_equiv to state the algebraic law (**) above and then prove
that it holds for your re_match function.

**********************************************************************)

Theorem seq_distr_alt: Admitted.

(**********************************************************************

QUESTION 8. List suffixes (10 marks)

A list l1 is a suffix of another list l2 if the elements of l1 occur at
the end of l2 in the same order. E.g., [3; 4] is a suffix of
[1; 2; 3; 4] but not of [1; 2; 3] or [1].

First, define an inductive relation (suffix l1 l2) that expresses when
a list of natural numbers l1 is a suffix of another list of natural
numbers l2.

**********************************************************************)

Inductive suffix {X : Type}: list X -> list X -> Prop := admit.                 

(*

Now state a theorem that your suffix relation is transitive and prove
it. In other words, show that if l1 is a suffix of l2 and l2 is a
suffix of l3, then l1 is a suffix of l3.

*)

Theorem suffix_trans: Admitted.

(**********************************************************************

QUESTION 9. Matching matches suffixes (20 marks)

Consider again the re_match function that you defined in Question 6.
When a match is successful, re_match returns the input that wasn't
matched by the regular expression.

We would hope that returned list is a suffix of the original input
list. State a theorem that expresses this property using your suffix
relation from Question 8 and prove that your re_match function has
this property.

**********************************************************************)

Theorem re_match_gives_suffix : Admitted.
